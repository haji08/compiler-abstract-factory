package org.compiler.generator;

import org.compiler.meta.AST;

import java.io.File;

public abstract class AbstractGenerator {
    public abstract File generate(AST a);
}
