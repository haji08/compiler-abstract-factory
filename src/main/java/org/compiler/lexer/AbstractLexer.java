package org.compiler.lexer;

import org.compiler.meta.ProgramText;
import org.compiler.meta.ScannedText;

public abstract class AbstractLexer {
    public abstract ScannedText scan(ProgramText t);
}
