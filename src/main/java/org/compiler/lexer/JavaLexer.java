package org.compiler.lexer;

import org.compiler.meta.ProgramText;
import org.compiler.meta.ScannedText;

public class JavaLexer extends AbstractLexer {
    @Override
    public ScannedText scan(ProgramText t) {
        System.out.println("I am scanning a JAVA text");
        return new ScannedText();
    }
}
