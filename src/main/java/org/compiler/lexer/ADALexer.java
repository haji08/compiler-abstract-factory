package org.compiler.lexer;

import org.compiler.meta.ProgramText;
import org.compiler.meta.ScannedText;

public class ADALexer extends AbstractLexer {
    @Override
    public ScannedText scan(ProgramText t) {
        System.out.println("I am scanning a ADA Text");
        return new ScannedText();
    }
}
