package org.compiler.parser;

import org.compiler.meta.AST;
import org.compiler.meta.ScannedText;

public abstract class AbstractParser {
    public abstract AST parse(ScannedText t);
}
