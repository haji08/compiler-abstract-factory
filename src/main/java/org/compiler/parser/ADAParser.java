package org.compiler.parser;

import org.compiler.meta.AST;
import org.compiler.meta.ScannedText;

public class ADAParser extends AbstractParser {
    @Override
    public AST parse(ScannedText t) {
        System.out.println("I am parsing a ADA text");
        return new AST();
    }
}
