package org.compiler;

import org.compiler.meta.Languages;
import org.factory.CompilerFactory;
import org.compiler.generator.AbstractGenerator;
import org.compiler.lexer.AbstractLexer;
import org.compiler.parser.AbstractParser;
import org.compiler.meta.AST;
import org.compiler.meta.ProgramText;
import org.compiler.meta.ScannedText;
import org.exception.UnSupportedLanguageException;

import java.util.Arrays;

public class Compiler {
    protected CompilerFactory factory;

    protected AbstractLexer lexer;
    protected AbstractParser parser;
    protected AbstractGenerator generator;
    String language;

    public Compiler(String language) throws Exception {
        this.factory = CompilerFactory.getFactory(language);

        this.lexer = factory.getLexer();
        this.parser = factory.getParser();
        this.generator = factory.getGenerator();

        this.language = language;
    }

    public void compile(ProgramText programText) throws UnSupportedLanguageException {
        if (!Arrays.asList(Languages.values()).contains(Languages.valueOf(this.language)))
            throw new UnSupportedLanguageException(this.language);

        ScannedText text = this.lexer.scan(programText);
        AST a = this.parser.parse(text);
        this.generator.generate(a);

        System.out.println("Your program is now compiled");
    }
}
