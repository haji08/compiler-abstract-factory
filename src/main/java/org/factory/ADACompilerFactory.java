package org.factory;

import org.compiler.generator.AbstractGenerator;
import org.compiler.lexer.AbstractLexer;
import org.compiler.parser.AbstractParser;
import org.compiler.generator.ADAGenerator;
import org.compiler.lexer.ADALexer;
import org.compiler.parser.ADAParser;

public class ADACompilerFactory extends CompilerFactory {

    @Override
    public AbstractLexer getLexer() {
        return new ADALexer();
    }

    @Override
    public AbstractParser getParser() {
        return new ADAParser();
    }

    @Override
    public AbstractGenerator getGenerator() {
        return new ADAGenerator();
    }
}
