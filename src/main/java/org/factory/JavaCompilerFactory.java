package org.factory;

import org.compiler.generator.AbstractGenerator;
import org.compiler.lexer.AbstractLexer;
import org.compiler.parser.AbstractParser;
import org.compiler.generator.JavaGenerator;
import org.compiler.lexer.JavaLexer;
import org.compiler.parser.JavaParser;

public class JavaCompilerFactory extends CompilerFactory {

    @Override
    public AbstractLexer getLexer() {
        return new JavaLexer();
    }

    @Override
    public AbstractParser getParser() {
        return new JavaParser();
    }

    @Override
    public AbstractGenerator getGenerator() {
        return new JavaGenerator();
    }
}
