package org.factory;

import org.compiler.generator.AbstractGenerator;
import org.compiler.lexer.AbstractLexer;
import org.compiler.parser.AbstractParser;
import org.compiler.generator.CPPGenerator;
import org.compiler.lexer.CPPLexer;
import org.compiler.parser.CPPParser;

public class CPPCompilerFactory extends CompilerFactory {

    @Override
    public AbstractLexer getLexer() {
        return new CPPLexer();
    }

    @Override
    public AbstractParser getParser() {
        return new CPPParser();
    }

    @Override
    public AbstractGenerator getGenerator() {
        return new CPPGenerator();
    }
}
