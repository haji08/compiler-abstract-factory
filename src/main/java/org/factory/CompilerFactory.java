package org.factory;

import org.compiler.generator.AbstractGenerator;
import org.compiler.lexer.AbstractLexer;
import org.compiler.parser.AbstractParser;

public abstract class CompilerFactory {

    public static CompilerFactory getFactory(String lang) throws Exception {
        Class<?> factoryClass = Class.forName(String.format("org.factory.%sCompilerFactory", lang));

        return (CompilerFactory)factoryClass.getDeclaredConstructor().newInstance();
    }

    public abstract AbstractLexer getLexer();

    public abstract AbstractParser getParser();

    public abstract AbstractGenerator getGenerator();
}
