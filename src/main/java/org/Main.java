package org;

import org.compiler.Compiler;
import org.compiler.meta.ProgramText;

public class Main {
    public static void main(String[] args) {
        try {
            System.out.println("-----------------------");
            Compiler javaCompiler = new Compiler("Java");
            javaCompiler.compile(new ProgramText("..."));
            System.out.println("-----------------------");
            Compiler cppCompiler = new Compiler("CPP");
            cppCompiler.compile(new ProgramText("..."));
            System.out.println("-----------------------");
            Compiler adaCompiler = new Compiler("ADA");
            adaCompiler.compile(new ProgramText("..."));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}