package org.exception;

public class UnSupportedLanguageException extends Exception{
    public UnSupportedLanguageException(String languageName) {
        super(String.format("Programming language %s is not supported", languageName));
    }
}
